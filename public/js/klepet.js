// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      
      var kanal = besede.join(' ');
      
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        if(parametri[3].includes("https://www.youtube.com/watch?v="))  {
          var tabelaSporocilo = parametri[3].split(" ");
          var tabelaPovezav = [];
          for(var t in tabelaSporocilo) {
            if(tabelaSporocilo[t].includes('https://www.youtube.com/watch?v=')) {
            tabelaPovezav.push(tabelaSporocilo[t].split('=').pop()); 
            }
          }
          var povezava = "";
          for(var y in tabelaPovezav) {
            povezava += "<iframe src='https://www.youtube.com/embed/" + tabelaPovezav[y] + "' allowfullscreen style='width: 200px; height: 150px; padding-left: 10px; padding-top: 10px; border: transparent'></iframe>";
            //console.log(povezava);
          }

            this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
            sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
            this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: povezava});

        }
        else  {
            this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
            sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];   
        }
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'barva':
      besede.shift();
      document.querySelector('#sporocila').style.color = besede[0];
      document.querySelector('#kanal').style.color = besede[0];
      break;
    case 'omemba':
      besede.shift();
      //var posiljatelj = besede[0];
      var prejemnik = besede[1];
      this.socket.emit('sporocilo', {vzdevek: prejemnik, besedilo: '\u261E Omemba v klepetu'});
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};